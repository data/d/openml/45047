# OpenML dataset: Airlines_DepDelay_1M

https://www.openml.org/d/45047

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dataset used in the tabular data benchmark https://github.com/LeoGrin/tabular-benchmark, transformed in the same way. This dataset belongs to the "regression on both numerical and categorical features" benchmark. 
 
  Original link: https://openml.org/d/42721 
 
 Original description: 
 
**Author**: Bureau of Transportation Statistics, Airline Service Quality Performance  
**Source**: [original](http://www.transtats.bts.gov/) - 2013  
**Please cite**:   

Airlines Departure Delay Prediction (Regression).
Original data can be found at: http://www.transtats.bts.gov

This is a processed version of the original data, designed to predict departure delay (in seconds).  

A CSV of the raw data (years 1987-2013) can be be found [here](https://h2o-airlines-unpacked.s3.amazonaws.com/allyears.1987.2013.csv). This is the first 1 million rows (and a subset of the columns) of this CSV file, in ARFF format.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45047) of an [OpenML dataset](https://www.openml.org/d/45047). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45047/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45047/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45047/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

